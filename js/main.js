"use strict"

const tabsService = document.querySelectorAll(".service-item");
const serviceInfos = document.querySelectorAll(".service-info");
tabsService.forEach(element => {
    for (const item of serviceInfos) {
        if(item.dataset.name === undefined){
            item.setAttribute("data-name",element.textContent);
            break;
        }
    }
    element.addEventListener("click", onServiceClick);
});

function onServiceClick(event) {
    const serviceList = document.querySelector(".service-list");
    serviceList.querySelector(".active").classList.remove("active");
    event.target.classList.add("active");
    const serviceInfoList = document.querySelector(".service-info-list")
    serviceInfoList.querySelector(".active").classList.remove("active");
    for (const item of serviceInfos) {
        if(event.target.textContent === item.dataset.name){
            item.classList.add("active");
            console.log(item);
        }
    }
}


const tabsWorks = document.querySelectorAll(".amazing-work-item");
tabsWorks.forEach((element)=>{
    element.addEventListener('click', onSortButtonClick);
})
function onSortButtonClick(event) {
    const workExamples = document.querySelectorAll(".amazing-work-example");
    document.querySelector(".amazing-work-list").querySelector(".active").classList.remove("active");
    event.target.classList.add("active");
    workExamples.forEach(element => {
        element.classList.remove("active");
        if(event.target.textContent === "All"){
            element.classList.add("active");
        }
        if(element.dataset.name === event.target.textContent){
            element.classList.add("active");
        }
    });
}
let countOfWorks = 0;
function loadNewWorks() {
    ++countOfWorks;
    const sectionAmazingWorks = document.querySelector(".amazing-work-examples");
    document.querySelector(".load-more").remove();
    const dotesWrap = document.createElement("div");
    dotesWrap.classList.add("loading");
    for (let i = 0; i < 5; i++) {
        const dot = document.createElement("div");
        dot.classList.add("dot");
        dotesWrap.append(dot);        
    }
    sectionAmazingWorks.after(dotesWrap);
    setTimeout(()=>{
        const fragment = document.createDocumentFragment();
    for (let i = 0; i < 12; i++) {
        const listItem = document.createElement("li");
        listItem.classList.add("amazing-work-example");
        listItem.classList.add("active");
        const image = document.createElement("img");
        if(i>=0&&i<3){
            listItem.setAttribute("data-name", "Graphic Design");
            if(countOfWorks>1){
                image.setAttribute("src", `./image/graphic-design${i+4}.jpg`);
            }else{
                image.setAttribute("src", `./image/graphic-design${i+1}.jpg`);
            }
        }
        if(i>=3&&i<6){
            listItem.setAttribute("data-name", "Web Design");
            if(countOfWorks>1){
                image.setAttribute("src", `./image/web-design${i+1}.jpg`);
            }else{
                image.setAttribute("src", `./image/web-design${i-2}.jpg`);
            }
        }
        if(i>=6&&i<10){
            listItem.setAttribute("data-name", "Wordpress");
            if(countOfWorks>1){
                image.setAttribute("src", `./image/wordpress${i-1}.jpg`);
            }else{
                image.setAttribute("src", `./image/wordpress${i-5}.jpg`);
            }
        }
        if(i>=10&&i<12){
            listItem.setAttribute("data-name", "Landing Pages");
            if(countOfWorks>1){
                image.setAttribute("src", `./image/landing-page${i-7}.jpg`);
            }else{
                image.setAttribute("src", `./image/landing-page${i-9}.jpg`);
            }
        }
        image.setAttribute("width","285");
        image.setAttribute("height", "206");
        image.setAttribute("alt", "")
        image.classList.add("example-image");
        listItem.append(image);
        const exampleText = document.createElement("div");
        exampleText.classList.add("example-text");
        const exampleLinks = document.createElement("div");
        exampleLinks.classList.add("example-icons-wrap");
        const exampleIconLinkFirst = document.createElement("a");
        exampleIconLinkFirst.setAttribute("href", "#");
        exampleIconLinkFirst.classList.add("example-text-link");
        const iconImageFirst = document.createElement("img");
        iconImageFirst.setAttribute("src", "./image/example-icon1.svg");
        iconImageFirst.setAttribute("width","43");
        iconImageFirst.setAttribute("height", "43");
        iconImageFirst.setAttribute("alt", "")
        iconImageFirst.classList.add("example-icon");
        exampleIconLinkFirst.append(iconImageFirst)
        exampleLinks.append(exampleIconLinkFirst);
        const exampleIconLinkSecond = document.createElement("a");
        exampleIconLinkSecond.setAttribute("href", "#");
        exampleIconLinkSecond.classList.add("example-text-link");
        const iconImageSecond = document.createElement("img");
        iconImageSecond.setAttribute("src", "./image/example-icon2.svg");
        iconImageSecond.setAttribute("width","43");
        iconImageSecond.setAttribute("height", "43");
        iconImageSecond.setAttribute("alt", "")
        iconImageSecond.classList.add("example-icon");
        exampleIconLinkSecond.append(iconImageSecond)
        exampleLinks.append(exampleIconLinkSecond);
        exampleText.append(exampleLinks);
        const title = document.createElement("p");
        title.classList.add("example-text-title");
        title.textContent = "creative design";
        exampleText.append(title);
        const type = document.createElement("p");
        type.classList.add("example-text-type");
        type.textContent = listItem.dataset.name;
        exampleText.append(type);
        listItem.append(exampleText);
        console.log(listItem);
        fragment.append(listItem);
    }
    sectionAmazingWorks.append(fragment);
    dotesWrap.remove();
    if(countOfWorks<2){
        const loadButton = document.createElement("button");
        loadButton.setAttribute("onclick", "loadNewWorks()");
        loadButton.classList.add("load-more");
        const loadText = document.createElement("p");
        loadText.textContent="Load more";
        loadText.classList.add("load");
        loadButton.append(loadText);
        sectionAmazingWorks.after(loadButton);
    }
    document.querySelector(".amazing-work-list").querySelector(".active").click();
    },1500);
    
}

document.querySelector(".load-more").addEventListener("click",loadNewWorks);


const commentators = document.querySelectorAll(".commentator-tab");
const commentList = document.querySelectorAll(".commentator");
commentators.forEach((element)=>{
    for (let i = 0; i < commentators.length; i++) {
        if(commentList[i].dataset.name === undefined){
            commentList[i].setAttribute("data-name", i+1);
        }
        if(commentators[i].dataset.name === undefined){
            commentators[i].firstElementChild.setAttribute("data-name", i+1);
        }
    }
    element.firstElementChild.addEventListener("click", onComentatorClick);
});

function onComentatorClick(event) {
    document.querySelector(".commentators-tabs").querySelector(".active").classList.remove("active");
    event.target.classList.add("active");
    chooseComment();
}
document.querySelector(".button-back").addEventListener('click', onArrowBackClick)
function onArrowBackClick() {
    for (let i = 1; i < commentators.length; i++) {
        if(commentators[i].firstElementChild.classList.contains('active')){
            commentators[i].firstElementChild.classList.remove('active');
            commentators[i-1].firstElementChild.classList.add('active')
            break;
        }
    }
    chooseComment();
}
document.querySelector(".button-forth").addEventListener('click', onArrowForthClick)
function onArrowForthClick() {
    for (let i = 0; i < commentators.length; i++) {
        if(commentators[i].firstElementChild.classList.contains('active') && commentators.length !== i+1 ){
            commentators[i].firstElementChild.classList.remove('active');
            commentators[i+1].firstElementChild.classList.add('active')
            break;
        }
    }
    chooseComment();
}

function chooseComment() {
    const tab = document.querySelector(".commentators-tabs").querySelector(".active");
    document.querySelector(".comment-list").querySelector(".active").classList.remove('active');
    commentList.forEach((element) => {
        if(element.dataset.name === tab.dataset.name){
            element.classList.add('active');
        }
    });
}